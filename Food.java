public class Food
{
    private String description;
    private int calories;
    
    public in getCalories(){
        return calories;
    }
    
    public void setCalories(int inCalories){
        calories = inCalories;
    }
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Set the description
     * @param inDescription The new description
     */
    public void setDescription(String inDescription)
    {
        description = inDescription;
    }
    
    @Overide
    public String toString(){
        return "Somebosy brought " + getDescription();
    }
    
}