public class Main
{
    public static void main(String[] args)
    {
        Food[] allTheFood = new Food[5];
        allTheFood[0] = new Wings;
        allTheFood[0].setDescription("Wings, but not spicy ones.");
        Wings wingsReference = (Wings)allTheFood[0];
        wingsReference.setRating(20);
        
        System.out.println(allTheFood[0].toString());
        System.out.println(allTheFood[1].toString());
        
        System.out.println("Let's have a party. People better bring a lot of food.");
    }
}